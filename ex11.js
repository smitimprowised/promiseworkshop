function all(first,second){
var counter = 0;
var output = [];
return new Promise(function (fulfill,reject){

first.then(function (input) {
      output[0] = input;
      ++counter;

      if (counter >= 2) {
        fulfill(output);
      }
    });
second.then(function (input) {
      output[1] = input;
      counter++;

      if (counter >= 2) {
        fulfill(output);
      }
    });
  
})
}

all(getPromise1(), getPromise2()).then(console.log);
