var promise = new Promise(function (fulfill, reject) {
  fulfill('SECRET VALUE');
});

// Introducing: Promise.resolve
// It does the exact same thing as above:

var promise = Promise.resolve('SECRET VALUE');


// Likewise...

var promise1 = new Promise(function (fulfill, reject) {
  reject(new Error('SECRET VALUE'));
}).catch(function(error){alert("The error is handled, continue normally");})


  