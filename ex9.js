function parsePromised(value){

	return new Promise(function(fulfill,reject){

		try {
			fulfill(JSON.parse(value));
		}
		catch(e){
			reject(e);
		}
	});
}

function onReject (error) {
  // Your solution here
  console.log(error.message);
}

// Your solution here
parsePromised(process.argv[2])
.then(null,onReject);